<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 */
?>
	<footer class="cf">
		<h2 class="fll"><img src="<?php bloginfo('template_url'); ?>/common/images/common/logo_foot.jpg" alt="フルーツパーラー" width="155" height="61"></h2>
		<div class="foot_deta">
			<p class="shop_name">フルーツパーラーむらはた</p>
			<p>&#12306;920-0855 金沢市武蔵町2-12<br>営業時間／10:00〜19:00(L.O 18:30)&emsp;TEL.<span>076-224-6800</span></p>
		</div>
	</footer>
<?php wp_footer(); ?>
</body>
</html>
