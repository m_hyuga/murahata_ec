<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<div id="content" class="menu">
		<h2><img src="<?php bloginfo('template_url'); ?>/common/images/news/ttl_h2_01.jpg" alt="NEWS" width="1001" height="134"></h2>
		<div class="cf">
			<div id="menu-list" class="fll">
				<div class="ttl_menu">
					<ul id="name-list" class="tbl">
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/lists/news/">新着情報一覧</a></li>
					</ul>
				</div>
				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<div class="news_datail cf">
				<h4 class="single_title"><?php the_title(); ?><span><?php the_time('Y/m/d'); ?></span></h4>
					<div class="mceContentBody">
					<?php the_content(); ?>
					</div>
				</div>
				<?php endwhile; endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>

<?php
get_footer();
