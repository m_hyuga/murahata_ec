<?php
	mb_internal_encoding("utf-8");
	require_once("../dbini_mht.php");
	
	/* とりあえずDB接続 */
	$con = mysql_connect($DBSERVER,$DBUSER,$DBPASSWORD);
	$selectdb = mysql_select_db($DBNAME,$con);
	// SET NAMES クエリの発行
	$sql = "SET NAMES utf8";
	$rst = mysql_query($sql,$con);
	
//	echo getItemList();
	
	echo uniqid("", TRUE);
	
	exit;
	

	/* アイテム一覧 */
	function getItemList(){
		global $con;
		
		
		$sql = "select ";
		$sql .= " count(mht_item.id) as cnt ";
		$sql .= " from mht_item ";
		$sql .= " left join mht_category ";
		$sql .= " on mht_item.category_id = mht_category.id ";
		$sql .= " where mht_category.type = 'news'";
		$rst = mysql_query($sql,$con);
		if($rst){
			$data = "<data>\n";
			$col = mysql_fetch_array($rst);
			$data .= "<count>\n";
			$data .= $col['cnt'];
			$data .= "</count>\n";
		}else{
			return "error";
		}
		
		$sql = "select ";
		$sql .= " mht_item.id as id, ";
		$sql .= " mht_item.title as title, ";
		$sql .= " mht_item.body as body, ";
		$sql .= " coalesce(aaa.filename,'') as filename, ";
		$sql .= " date_format(mht_item.date,'%Y年%c月') as date, ";
		$sql .= " mht_item.alive as alive ";
		$sql .= " from (mht_item ";
		$sql .= " left join mht_category ";
		$sql .= " on mht_category.id = mht_item.category_id) ";
		$sql .= " left join (select item_id,filename from mht_images where order_no = 0) as aaa ";
		$sql .= " on mht_item.id = aaa.item_id ";
		$sql .= " where mht_category.type = 'news'";
		$sql .= " order by date desc,id desc ";
		
		$rst = mysql_query($sql,$con);
		if($rst){
			while($col = mysql_fetch_array($rst)){
				$data .= "<itemdata>\n";
				foreach($col as $key => $value){
					if(!is_numeric($key)){
						$data .= "<".$key.">".$value."</".$key.">\n";
					}
				}
				$data .= "</itemdata>\n";
			}
			$data .= "</data>\n";
			return $data;
		}else{
			return "error";
		}
	}

?>