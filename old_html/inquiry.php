<?php
	mb_language("Japanese");
	mb_internal_encoding("UTF-8");
	mb_detect_order("UTF-8,EUC-JP,SJIS,ASCII,JIS");
	
	
	if($_POST['key'] != "mht"){
		echo "error";
	}else{
		$frommailaddress = $_POST['mailaddress'];
		$mail_title = 'ウェブサイトからお問合せがありました'; //お問合せタイトル
		$tomailaddress = "shop@murahata.co.jp"; //送信先
//		$tomailaddress = "kihara.kkf@gmail.com"; //送信先
//		$tomailaddress = "kihara@topaz.plala.or.jp"; //送信先
		
		
		$mail_body="━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n"
		."□ ウェブサイトからお問合せがありました □\n"
		."\n"
		.date("Y/m/d")."\n"
		."━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n"
		."\n"
		."以下のお問合せがありました。内容は以下のとおりです。\n"
		."\n"
		."\n"
		."●ウェブサイトからのお問合せ内容●\n"
		."──────────────────────────────\n"
		."■会社名\n"
		."　".$_POST['company']."\n"
		."──────────────────────────────\n"
		."■お名前\n"
		."　".$_POST['name']."\n"
		."──────────────────────────────\n"
		."■フリガナ\n"
		."　".$_POST['ruby']."\n"
		."──────────────────────────────\n"
		."■電話番号\n"
		."　".$_POST['tel']."\n"
		."──────────────────────────────\n"
		."■メールアドレス\n"
		."　".$frommailaddress."\n"
		."──────────────────────────────\n"
		."■お問合せ内容\n"
		.$_POST['body']."\n"
		."──────────────────────────────\n"
		."\n"
		."以上\n";
		
		if(sendToMail($frommailaddress,$tomailaddress,$mail_title,$mail_body)){
			echo "ok";
		}else{
			echo "error";
		}
	}
	
	
	

	
	function sendToMail($frommailaddress,$tomailaddress,$mail_title,$mail_body){
		//メール送信
		$newbody = mb_convert_encoding($mail_body,"ISO-2022-JP");
		$newbody = ereg_replace("\r","\r\n",$newbody);
		

		$header   = "From: ".$frommailaddress."\n";
		$header  .= "Reply-To: ".$frommailaddress."\n";
		$header  .= "X-Mailer: myphpMail ". phpversion(). "\n";
//		return mb_send_mail($tomailaddress, $mail_title, $newbody, $header,'-f shop@murahata.co.jp');
		return mb_send_mail($tomailaddress, $mail_title, $newbody, "From: ".$frommailaddress);
//		return mb_send_mail($tomailaddress, $mail_title, $newbody, $header,'-f '.$frommailaddress);
//		return mail($tomailaddress, $mail_title, $newbody, $header);
	}
	
	function sendToMail2($frommailaddress,$tomailaddress,$mailtitle,$mailbody){
		//デバッグ用
		$mail_address = $tomailaddress;
		$mail_name = "";
		$mail_subject= $mailtitle;
		$mail_header = "From:".mb_encode_mimeheader(mb_convert_encoding($mail_name,'JIS','UTF-8'))." <".$frommailaddress.">";
		$mail_body = mb_convert_encoding($mailbody,'JIS','auto');
		
		$data = "------------------------------------\n";
		$data .= date("Y/m/d l H:i:s")."\n";
		$data .= "from_address:".$frommailaddress."\n";
		$data .= "to_address:".$tomailaddress."\n";
		$data .= "title:".$mailtitle."\n";
		$data .= "body:".$mailbody."\n";
		
		$filname = "./log.log";
		$fp = fopen($filname,"a");
		flock($fp, LOCK_EX);
		if($fp){
		  fwrite($fp,$data."\n");
		}
		fclose($fp);
		
		return true;
	}
?>