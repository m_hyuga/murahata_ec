<!--{*
 * 
 * This file is part of Japan Electronic Industrial Arts
 * 
 * Copyright(c) 2011 Japan Electronic Industrial Arts CO.,LTD. All Rights Reserved.
 * 
 * http://www.jeia.co.jp/
 * 
 *}-->

<!--{* #ecp_HistoryList *}-->
<!--{if count($arrStatusBloc) > 0}-->
<link rel="stylesheet" href="/plugin/Status4Bloc/media/Status4Bloc.css" type="text/css" media="all" />
	<div class="block_outer">
		<div id="ecp_Status4Bloc">
			<h2>おすすめ商品</h2>
			<div class="block_body">
				<div class="product_list">
					<div class="clearfix">
<!--{foreach from=$arrStatusBloc item=arrProduct name="ecp_Status4Bloc"}-->
						<div class="product_item<!--{if $smarty.foreach.ecp_Status4Bloc.last}--> ecp_last_child<!--{/if}-->">
							<div class="productImage">
								<a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
									<img src="<!--{$smarty.const.ROOT_URLPATH}-->resize_image.php?image=<!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->&amp;width=120&amp;height=120" alt="<!--{$arrProduct.name|h}-->" />
								</a>
							</div>
							<div class="productContents">
								<h3>
									<a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->"><!--{$arrProduct.name|h}--></a>
								</h3>
							</div>
                            <p class="sale_price">
                                <!--{$smarty.const.SALE_PRICE_TITLE}-->(税込)： <span class="price"><!--{$arrProduct.price02_min_inctax|number_format}--> 円</span>
                            </p>
						</div>
<!--{/foreach}-->
					</div>
				</div>
			</div>
		</div>
	</div>
<!--{/if}-->
