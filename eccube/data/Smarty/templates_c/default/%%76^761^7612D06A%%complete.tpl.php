<?php /* Smarty version 2.6.27, created on 2015-03-27 10:21:38
         compiled from /var/www/vhosts/demdm.net/httpdocs/murahata_ec/ec/data/Smarty/templates/default/contact/complete.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'script_escape', '/var/www/vhosts/demdm.net/httpdocs/murahata_ec/ec/data/Smarty/templates/default/contact/complete.tpl', 25, false),array('modifier', 'h', '/var/www/vhosts/demdm.net/httpdocs/murahata_ec/ec/data/Smarty/templates/default/contact/complete.tpl', 25, false),)), $this); ?>

<div id="undercolumn">
		<div id="main_area" class="other_page">
    <h2><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['tpl_title'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
</h2>
    <div id="undercolumn_contact">
        <div id="complete_area">
            <p class="message">お問い合わせ内容の送信が完了いたしました。</p>
            <p>
                万一、ご回答メールが届かない場合は、トラブルの可能性もありますので<br />大変お手数ではございますがもう一度お問い合わせいただくか、お電話にてお問い合わせください。<br />
                今後ともご愛顧賜りますようよろしくお願い申し上げます。
            </p>
            <div class="shop_information">
            <p class="name"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['company_name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
<br />
            <p>
								フルーツむらはた<br>
								TEL：076-224-6800<br>
                (営業時間 9:00〜18:30)
            </p>
            </div>

            <div class="btn_area">
                <ul>
                <li>
                    <a href="<?php echo ((is_array($_tmp=@TOP_URL)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
" class="prev_button">トップページへ</a>
                </li>
                </ul>
            </div>
        </div>
    </div>
    </div>
</div>