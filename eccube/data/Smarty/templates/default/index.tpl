<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--{strip}-->
		<div class="cf">
			<div class="facebook"><iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Ffruitmurahata%3Ffref%3Dts&amp;width=236&amp;height=338&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=true&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:236px; height:338px;" allowtransparency="true"></iframe></div>
			<div class="main_v">
				<ul id="bxslider">
					<li><a href="<!--{$smarty.const.TOP_URL}-->concept/"><img src="<!--{$TPL_URLPATH}-->img/top/main_slide_01.jpg" alt="slide" width="760" height="230"></a></li>
				</ul>
				<div class="parlor"><a href="http://demdm.net/murahata_ec/wp/" target="_blank"><img src="<!--{$TPL_URLPATH}-->img/top/btn_parlor.jpg" alt="フルーツむらはたパーラー" width="760" height="125"></a></div>
			</div>
		</div>
		<div class="rec_menu cf">
			<div class="reccomend">
				<h3><img src="<!--{$TPL_URLPATH}-->img/top/ttl_rec.jpg" alt="おすすめ商品" width="127" height="35"></h3>
                    <!--{* ▼メイン下部 *}-->
                    <!--{if $arrPageLayout.MainFoot|@count > 0}-->
                        <!--{foreach key=MainFootKey item=MainFootItem from=$arrPageLayout.MainFoot}-->
                            <!-- ▼<!--{$MainFootItem.bloc_name}--> -->
                            <!--{if $MainFootItem.php_path != ""}-->
                                <!--{include_php file=$MainFootItem.php_path items=$MainFootItem}-->
                            <!--{else}-->
                                <!--{include file=$MainFootItem.tpl_path items=$MainFootItem}-->
                            <!--{/if}-->
                            <!-- ▲<!--{$MainFootItem.bloc_name}--> -->
                        <!--{/foreach}-->
                    <!--{/if}-->
                    <!--{* ▲メイン下部 *}-->
			</div><!-- /reccomend -->
			<nav class="cake_menu">
				<div class="cf">
					<h3><img src="<!--{$TPL_URLPATH}-->img/top/ttl_cakes.jpg" alt="洋菓子メニュー" width="99" height="35"></h3>
					<p class="cakes_list"><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1">洋菓子メニュー 一覧へ</a></p>
				</div>
				<ul class="cf">
				<!--{include_php file=`$smarty.const.HTML_REALDIR`frontparts/bloc/product_list.php}-->
				</ul>
			</nav>
		</div>
<!--{/strip}-->
