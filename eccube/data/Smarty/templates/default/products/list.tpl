<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<script type="text/javascript">//<![CDATA[
    function fnSetClassCategories(form, classcat_id2_selected) {
        var $form = $(form);
        var product_id = $form.find('input[name=product_id]').val();
        var $sele1 = $form.find('select[name=classcategory_id1]');
        var $sele2 = $form.find('select[name=classcategory_id2]');
        eccube.setClassCategories($form, product_id, $sele1, $sele2, classcat_id2_selected);
    }
    // 並び順を変更
    function fnChangeOrderby(orderby) {
        eccube.setValue('orderby', orderby);
        eccube.setValue('pageno', 1);
        eccube.submitForm();
    }
    // 表示件数を変更
    function fnChangeDispNumber(dispNumber) {
        eccube.setValue('disp_number', dispNumber);
        eccube.setValue('pageno', 1);
        eccube.submitForm();
    }
    // カゴに入れる
    function fnInCart(productForm) {
        var searchForm = $("#form1");
        var cartForm = $(productForm);
        // 検索条件を引き継ぐ
        var hiddenValues = ['mode','category_id','maker_id','name','orderby','disp_number','pageno','rnd'];
        $.each(hiddenValues, function(){
            // 商品別のフォームに検索条件の値があれば上書き
            if (cartForm.has('input[name='+this+']').length != 0) {
                cartForm.find('input[name='+this+']').val(searchForm.find('input[name='+this+']').val());
            }
            // なければ追加
            else {
                cartForm.append($('<input type="hidden" />').attr("name", this).val(searchForm.find('input[name='+this+']').val()));
            }
        });
        // 商品別のフォームを送信
        cartForm.submit();
    }
//]]></script>

		<div class="cf">
		<div class="side_area">
			<div class="facebook"><iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Ffruitmurahata%3Ffref%3Dts&amp;width=236&amp;height=338&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=true&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:236px; height:338px;" allowtransparency="true"></iframe></div>
			<div class="rec_menu">
			<div class="reccomend">
				<h3><img src="<!--{$TPL_URLPATH}-->img/top/ttl_rec.jpg" alt="おすすめ商品" width="127" height="35"></h3>
                    <!--{* ▼メイン下部 *}-->
                    <!--{if $arrPageLayout.MainFoot|@count > 0}-->
                        <!--{foreach key=MainFootKey item=MainFootItem from=$arrPageLayout.MainFoot}-->
                            <!-- ▼<!--{$MainFootItem.bloc_name}--> -->
                            <!--{if $MainFootItem.php_path != ""}-->
                                <!--{include_php file=$MainFootItem.php_path items=$MainFootItem}-->
                            <!--{else}-->
                                <!--{include file=$MainFootItem.tpl_path items=$MainFootItem}-->
                            <!--{/if}-->
                            <!-- ▲<!--{$MainFootItem.bloc_name}--> -->
                        <!--{/foreach}-->
                    <!--{/if}-->
                    <!--{* ▲メイン下部 *}-->
			</div>
			</div><!-- /reccomend -->
		</div>
		
		<div id="main_area" class="detail">
			<!--{if $arrSearchData.category_id == 1}-->
					<h3><img src="<!--{$TPL_URLPATH}-->img/cakes/ttl_main.jpg" alt="洋菓子メニュー"></h3>
				<div class="ttl_cate cf">
					<h4><img src="<!--{$TPL_URLPATH}-->img/common/ttl_comm_list.jpg" alt="商品一覧" width="79" height="19"></h4>
				</div>
				<!--{elseif $arrSearchData.category_id == 6 || $arrSearchData.category_id == 7 || $arrSearchData.category_id == 8 }-->
					<h3><img src="<!--{$TPL_URLPATH}-->img/gift/ttl_main.jpg" alt="ショッピングメニュー"></h3>
				<div class="ttl_cate cf">
					<h4><img src="<!--{$TPL_URLPATH}-->img/common/ttl_comm_list.jpg" alt="商品一覧" width="79" height="19"></h4>
					<ul class="cate_list cf">
						<li><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=7"><span class="red_rarr">&#x25B6;</span>産直商品</a></li>
						<li><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=8"><span class="red_rarr">&#x25B6;</span>関連商品</a></li>
					</ul>
				</div>
			<!--{/if}-->
			<nav class="gift_menu">
			<form name="form1" id="form1" method="get" action="?">
					<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
					<input type="hidden" name="mode" value="<!--{$mode|h}-->" />
					<!--{* ▼検索条件 *}-->
					<input type="hidden" name="category_id" value="<!--{$arrSearchData.category_id|h}-->" />
					<input type="hidden" name="maker_id" value="<!--{$arrSearchData.maker_id|h}-->" />
					<input type="hidden" name="name" value="<!--{$arrSearchData.name|h}-->" />
					<!--{* ▲検索条件 *}-->
					<!--{* ▼ページナビ関連 *}-->
					<input type="hidden" name="orderby" value="<!--{$orderby|h}-->" />
					<input type="hidden" name="disp_number" value="<!--{$disp_number|h}-->" />
					<input type="hidden" name="pageno" value="<!--{$tpl_pageno|h}-->" />
					<!--{* ▲ページナビ関連 *}-->
					<input type="hidden" name="rnd" value="<!--{$tpl_rnd|h}-->" />
			</form>
			<!--▼ページナビ(本文)-->
			<!--{capture name=page_navi_body}-->
				<div class="pagenumber_area clearfix">
						<div class="change">
								<!--{if $orderby != 'price'}-->
										<a href="javascript:fnChangeOrderby('price');">価格順</a>
								<!--{else}-->
										<strong>価格順</strong>
								<!--{/if}-->&nbsp;
								<!--{if $orderby != "date"}-->
												<a href="javascript:fnChangeOrderby('date');">新着順</a>
								<!--{else}-->
										<strong>新着順</strong>
								<!--{/if}-->
								表示件数
								<select name="disp_number" onChange="javascript:fnChangeDispNumber(this.value);">
										<!--{foreach from=$arrPRODUCTLISTMAX item="dispnum" key="num"}-->
												<!--{if $num == $disp_number}-->
														<option value="<!--{$num}-->" selected="selected" ><!--{$dispnum}--></option>
												<!--{else}-->
														<option value="<!--{$num}-->" ><!--{$dispnum}--></option>
												<!--{/if}-->
										<!--{/foreach}-->
								</select>
						</div>
						<div class="navi"><!--{$tpl_strnavi}--></div>
				</div>
			<!--{/capture}-->
			<!--▲ページナビ(本文)-->

				<ul class="cf">

    <!--{foreach from=$arrProducts item=arrProduct name=arrProducts}-->
        <!--{assign var=id value=$arrProduct.product_id}-->
        <!--{assign var=arrErr value=$arrProduct.arrErr}-->
        <!--▼商品-->
				<li>
        <form name="product_form<!--{$id|h}-->" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="product_id" value="<!--{$id|h}-->" />
            <input type="hidden" name="product_class_id" id="product_class_id<!--{$id|h}-->" value="<!--{$tpl_product_class_id[$id]}-->" />
					<a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
						<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" width="" height=""><p><!--{$arrProduct.name|h}--><br>¥<!--{strip}--><!--{$arrProduct.price02_min_inctax|n2s}--><!--{/strip}--></p>
					</a>
        </form>
				</li>
        <!--▲商品-->

        <!--{if $smarty.foreach.arrProducts.last}-->
				</ul>
            <!--▼ページナビ(下部)-->
            <form name="page_navi_bottom" id="page_navi_bottom" action="?">
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <!--{if $tpl_linemax > 0}--><!--{$smarty.capture.page_navi_body|smarty:nodefaults}--><!--{/if}-->
            </form>
            <!--▲ページナビ(下部)-->
        <!--{/if}-->

    <!--{foreachelse}-->
        <!--{include file="frontparts/search_zero.tpl"}-->
    <!--{/foreach}-->
			</nav>
		</div><!-- main_area -->

	</div>

