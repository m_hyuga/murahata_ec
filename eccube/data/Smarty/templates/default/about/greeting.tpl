	<div id="content" class="cf">
		<h3><img src="<!--{$TPL_URLPATH}-->img/about/ttl_main.jpg" alt="about 会社概要"></h3>
		<div class="side_area">
			<div class="ttl_cate cf">
				<ul class="cate_list cf">
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/"><span class="red_rarr">&#x25B6;</span>会社概要</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/greeting.php"><span class="red_rarr">&#x25B6;</span>代表ご挨拶</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/history.php"><span class="red_rarr">&#x25B6;</span>沿革</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/story.php"><span class="red_rarr">&#x25B6;</span>会社ストーリー</a></li>
				</ul>
			</div>
		</div>

		<div id="main_area">
			<ul id="about_area">
				<li class="greeting">
					<h4>代表ご挨拶</h4>
					<p class="img_catch"><img src="<!--{$TPL_URLPATH}-->img/about/img_about01.jpg" alt="フルーツは日本の文化であり、価値を与えるものである" width="495" height="19"></p>
					<p class="txt_deta">弊社はフルーツを日本の文化の一部として位置づけ、フルーツを取り扱う者としてフルーツの美味しさだけではなく、美しさ、贈り物としての価値、デザートとしての価値、充実した生活空間での必要性を追求することを使命としております。近年、日本のフルーツはその品質を海外から認められ広く求められるようになりました。その素晴らしいフルーツを扱う立場として、日本の文化としてのフルーツの価値創造を担うことを使命とし、国内外を問わず、その販売を通じ社会に必要とされる存在であり続けようと考えております。</p>
					<p class="reader">代表取締役社長<br>村端　一男</p>
				</li>
			</ul>
		</div><!-- main_area -->

	</div>
