<!--{*
 * 
 * This file is part of Japan Electronic Industrial Arts
 * 
 * Copyright(c) 2011 Japan Electronic Industrial Arts CO.,LTD. All Rights Reserved.
 * 
 * http://www.jeia.co.jp/
 * 
 *}-->

<!--{* #ecp_HistoryList *}-->
<!--{if count($arrStatusBloc) > 0}-->
		<div class="slide">
			<ul id="bxslider_02">
<!--{foreach from=$arrStatusBloc item=arrProduct name="ecp_Status4Bloc"}-->
				<li>
					<div><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
					<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="slide">
					<p><!--{$smarty.const.SALE_PRICE_TITLE}--><br>¥<!--{$arrProduct.price02_min_inctax|n2s}--></p>
					</a></div>
				</li>
<!--{/foreach}-->
			</ul>
		</div>
<!--{/if}-->
